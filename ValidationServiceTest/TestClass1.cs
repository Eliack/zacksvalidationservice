﻿// <copyright file="TestClass1.cs" company="Zachary Joubert">
//    Copyright (c) Zachary Joubert All rights reserved.
// </copyright>
namespace ValidationServiceTest
{
    using ValidationService.ValidationAttributes;

    /// <summary>
    /// This is the test class to test the validation service
    /// </summary>
    public class TestClass1
    {
        /// <summary>
        /// Gets or sets an object to test required object
        /// </summary>
        [ValidateRequiredObject]
        public object RequiredObject { get; set; }

        /// <summary>
        /// Gets or sets a string to test maximum length
        /// </summary>
        [ValidateMaximumLength(50)]
        public string MaximumLength { get; set; }

        /// <summary>
        /// Gets or sets a string to test minimum length
        /// </summary>
        [ValidateMinimumLength(5)]
        public string MinimumLength { get; set; }

        /// <summary>
        /// Gets or sets an integer to test a minimum value
        /// </summary>
        [ValidateMinimumInteger(5)]
        public int MinimumInteger { get; set; }

        /// <summary>
        /// Gets or sets a string to test Lookup
        /// </summary>
        [ValidateLookUp("Gender")]
        public string Lookup { get; set; }

        /// <summary>
        /// Gets or sets an integer to test an integer lookup.
        /// </summary>
        [ValidateLookUp("IntegerTest")]
        public int LookUpInt { get; set; }
    }
}