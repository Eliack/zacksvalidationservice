﻿// <copyright file="ValidationServiceTests.cs" company="Zachary Joubert">
//    Copyright (c) Zachary Joubert All rights reserved.
// </copyright>
namespace ValidationServiceTest
{
    using System.Collections.Generic;
    using NUnit.Framework;
    using ValidationService;

    /// <summary>
    /// The Test class for the ValidationService
    /// </summary>
    [TestFixture]
    public class ValidationServiceTests
    {
        /// <summary>
        /// Gets or sets a new instance of <see cref="ValidationService"/> class.
        /// </summary>
        private ValidationService _validationService { get; set; }

        /// <summary>
        /// Gets or sets a new instance of <see cref="TestClass1"/> class.
        /// </summary>
        private TestClass1 _theGood { get; set; }

        /// <summary>
        /// Gets or sets a new instance of <see cref="TestClass1"/> class.
        /// </summary>
        private TestClass1 _theBad { get; set; }

        /// <summary>
        /// The Setup Method
        /// </summary>
        [SetUp]
        public void Setup()
        {
            List<object> genderList = new List<object>();

            genderList.Add("Male");
            genderList.Add("Female");
            genderList.Add("Other");

            List<object> intList = new List<object>();

            intList.Add(1);
            intList.Add(2);
            intList.Add(3);

            Dictionary<string, List<object>> lookUpDictionary = new Dictionary<string, List<object>>();

            lookUpDictionary.Add("Gender", genderList);
            lookUpDictionary.Add("IntegerTest", intList);

            this._validationService = new ValidationService(lookUpDictionary);

            this._theGood = new TestClass1()
            {
                Lookup = "Male",
                MaximumLength = "Under 50",
                MinimumInteger = 7,
                MinimumLength = "Over 5 Characters",
                RequiredObject = "I am required",
                LookUpInt = 1
            };

            this._theBad = new TestClass1()
            {
                Lookup = "Unicorn",
                MaximumLength = "And on this day we exclaim: We will not go quietly into the night! We will not vanish without a fight!",
                MinimumInteger = 0,
                MinimumLength = string.Empty,
                RequiredObject = null,
                LookUpInt = 5
            };
        }

        /// <summary>
        /// The Teardown Method for the Tests
        /// </summary>
        [TearDown]
        public void Teardown()
        {
            this._validationService = null;
            this._theGood = null;
            this._theBad = null;
        }

        /// <summary>
        /// Validation Sucess Test
        /// </summary>
        [Test]
        public void Validate_Sucess()
        {
            var result = this._validationService.Validate(this._theGood);

            Assert.That(result.Sucess, Is.EqualTo(true));
            Assert.That(result.ErrorList.Count, Is.EqualTo(0));
        }

        /// <summary>
        /// Validation Failure Test
        /// </summary>
        [Test]
        public void Validate_Fail()
        {
            var result = this._validationService.Validate(this._theBad);

            Assert.That(result.Sucess, Is.EqualTo(false));
            Assert.That(result.ErrorList.Count, Is.AtLeast(1));
        }
    }
}