﻿// <copyright file="ValidateMinimumLength.cs" company="Zachary Joubert">
//    Copyright (c) Zachary Joubert All rights reserved.
// </copyright>
namespace ValidationService.ValidationAttributes
{
    using System;

    /// <summary>
    /// This class is a property to have the validation service check for a minimum integer.
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]
    public class ValidateMinimumLength : Attribute
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ValidateMinimumLength"/> class.
        /// </summary>
        /// <param name="minimumLength">Sets the Minimum Length allowed.</param>
        public ValidateMinimumLength(int minimumLength)
        {
            this.MinimumLength = minimumLength;
        }

        /// <summary>
        /// Gets or sets an int for the Minimum Length
        /// </summary>
        public int MinimumLength { get; set; }
    }
}