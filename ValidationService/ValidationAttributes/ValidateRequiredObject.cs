﻿// <copyright file="ValidateRequiredObject.cs" company="Zachary Joubert">
//    Copyright (c) Zachary Joubert All rights reserved.
// </copyright>
namespace ValidationService.ValidationAttributes
{
    using System;

    /// <summary>
    /// This class is the property that allows the ValidationService to check for the required object
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]
    public class ValidateRequiredObject : Attribute
    {
    }
}