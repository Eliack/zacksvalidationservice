﻿// <copyright file="ValidateMaximumLength.cs" company="Zachary Joubert">
//    Copyright (c) Zachary Joubert All rights reserved.
// </copyright>
namespace ValidationService.ValidationAttributes
{
    using System;

    /// <summary>
    /// Validate Maximum Length attribute class
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]
    public class ValidateMaximumLength : Attribute
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ValidateMaximumLength"/> class.
        /// </summary>
        /// <param name="maximumLength">The maximum length of the string</param>
        public ValidateMaximumLength(int maximumLength)
        {
            this.MaximumLength = maximumLength;
        }

        /// <summary>
        /// Gets the MaximumLength allowed for the string
        /// </summary>
        public int MaximumLength { get; }
    }
}