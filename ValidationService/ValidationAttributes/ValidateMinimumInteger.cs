﻿// <copyright file="ValidateMinimumInteger.cs" company="Zachary Joubert">
//    Copyright (c) Zachary Joubert All rights reserved.
// </copyright>
namespace ValidationService.ValidationAttributes
{
    using System;

    /// <summary>
    /// This class is the property to have the ValidationService checks the Minimum integer
    /// </summary>
    public class ValidateMinimumInteger : Attribute
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ValidateMinimumInteger"/> class.
        /// </summary>
        /// <param name="minimumInteger">Sets the minimum integer.</param>
        public ValidateMinimumInteger(int minimumInteger)
        {
            this.MinimumInteger = minimumInteger;
        }

        /// <summary>
        /// Gets or sets Minimum Integer
        /// </summary>
        public int MinimumInteger { get; set; }
    }
}