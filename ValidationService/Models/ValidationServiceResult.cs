﻿// <copyright file="ValidationServiceResult.cs" company="Zachary Joubert">
//    Copyright (c) Zachary Joubert All rights reserved.
// </copyright>
namespace ValidationService.Models
{
    using System.Collections.Generic;

    /// <summary>
    /// A class that is a model for the Validation Service Result
    /// </summary>
    public class ValidationServiceResult
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ValidationServiceResult"/> class.
        /// </summary>
        public ValidationServiceResult()
        {
            this.ErrorList = new List<ValidationError>();
        }

        /// <summary>
        /// Gets or sets a value indicating whether it was successful.
        /// </summary>
        public bool Sucess { get; set; }

        /// <summary>
        /// Gets or sets the list of errors
        /// </summary>
        public List<ValidationError> ErrorList { get; set; } 
    }
}