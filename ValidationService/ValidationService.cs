﻿// <copyright file="ValidationService.cs" company="Zachary Joubert">
//    Copyright (c) Zachary Joubert All rights reserved.
// </copyright>
namespace ValidationService
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using Models;
    using ValidationAttributes;

    /// <summary>
    /// Validation Service class
    /// </summary>
    public class ValidationService
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ValidationService"/> class.
        /// </summary>
        public ValidationService()
        {
            this.Lists = new Dictionary<string, List<object>>();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ValidationService"/> class.
        /// </summary>
        /// <param name="lists">A Dictionary that holds the looks ups and uses a name to identify them.</param>
        public ValidationService(Dictionary<string, List<object>> lists)
        {
            this.Lists = lists;
        }

        /// <summary>
        /// Gets or sets a Dictionary of Lists
        /// </summary>
        public Dictionary<string, List<object>> Lists { get; set; }

       #region Public Methods

        /// <summary>
        /// This Validates an instance of Class {T}
        /// </summary>
        /// <typeparam name="T">The type of object to be Validated</typeparam>
        /// <param name="entity">The instance of {T} to be Validated</param>
        /// <returns>Returns a ValidationServiceResult that contains a list of errors and a sucess boolean parameter</returns>
        public ValidationServiceResult Validate<T>(T entity)
        {
            ValidationServiceResult serviceResult = new ValidationServiceResult();

            PropertyInfo[] props = typeof(T).GetProperties();

            foreach (var propertyInfo in props)
            {
                var propertyName = propertyInfo.Name;
                var tempValue = propertyInfo.GetValue(entity);

                string propertyValue = null;

                if (tempValue != null)
                {
                    propertyValue = tempValue.ToString();
                }

                var attributes = propertyInfo.GetCustomAttributes(true).ToDictionary(a => a.GetType().Name, a => a);

                foreach (KeyValuePair<string, object> attribute in attributes)
                {
                    var propertyAttribute = attribute.Value;
                    var attributeName = propertyAttribute.ToString().Split('.')[2];

                    switch (attributeName)
                    {
                        case "ValidateMaximumLength":

                            var maximumLengthAttribute = entity.GetType().GetTypeInfo().GetProperty(propertyName).GetCustomAttribute<ValidateMaximumLength>();
                            this.ValidateMaximumString(propertyName, propertyValue, maximumLengthAttribute.MaximumLength, serviceResult.ErrorList);
                            break;
                        case "ValidateMinimumInteger":

                            var minimumIntegerAttribute = entity.GetType().GetTypeInfo().GetProperty(propertyName).GetCustomAttribute<ValidateMinimumInteger>();
                            this.ValidateMinimumInteger(propertyName, Convert.ToInt32(propertyValue), minimumIntegerAttribute.MinimumInteger, serviceResult.ErrorList);
                            break;
                        case "ValidateMinimumLength":

                            var minimumLengthAttribute = entity.GetType().GetTypeInfo().GetProperty(propertyName).GetCustomAttribute<ValidateMinimumLength>();
                            this.ValidateMinimumString(propertyName, propertyValue, minimumLengthAttribute.MinimumLength, serviceResult.ErrorList);
                            break;
                        case "ValidateRequiredObject":

                            this.ValidateRequiredObject(propertyName, propertyValue, serviceResult.ErrorList);
                            break;
                        case "ValidateLookUp":

                            var validateLookUpAttribute = entity.GetType().GetTypeInfo().GetProperty(propertyName).GetCustomAttribute<ValidateLookUp>();
                            this.ValidateLookUp(propertyName, propertyValue, validateLookUpAttribute.ListName, serviceResult.ErrorList);
                            break;
                    }
                }
            }

            if (serviceResult.ErrorList.Count > 0)
            {
                serviceResult.Sucess = false;
            }
            else
            {
                serviceResult.Sucess = true;
            }

            return serviceResult;
        }
        
        /// <summary>
        /// This function check to see if the string has equal or fewer characters to validate.
        /// </summary>
        /// <param name="propertyName">The name of the property</param>
        /// <param name="propertyValue">The value of the property</param>
        /// <param name="maximumLength">The maximum length of the string that is allowed set by the attribute</param>
        /// <param name="validationErrors">A reference to a list of type ValidationError</param>
        /// <returns>Returns true or false depending if it properly validates</returns>
        public bool ValidateMaximumString(string propertyName, string propertyValue, int maximumLength, List<ValidationError> validationErrors)
        {
            if (propertyValue == null)
            {
                return true;
            }

            if (propertyValue.Length <= maximumLength)
            {
                return true;
            }
            else
            {
                validationErrors.Add(this.GenerateValidationError(propertyName, string.Format("The value of {0} is too long. The maximum characters allow is {1}.", propertyName, maximumLength)));
                return false;
            }
        }

        /// <summary>
        /// Validates an integer to ensure that it is greater than the value passed to it. 
        /// </summary>
        /// <param name="propertyName">The name of the property</param>
        /// <param name="propertyValue">The value of the property</param>
        /// <param name="minimumValue">The minimum value that is allowed set by the attribute</param>
        /// <param name="validationErrors">A reference to a list of type ValidationError</param>
        /// <returns>Returns true or false depending if it properly validates</returns>
        public bool ValidateMinimumInteger(string propertyName, int? propertyValue, int minimumValue, List<ValidationError> validationErrors)
        {
            if (propertyValue == null)
            {
                return true;
            }

            if (propertyValue >= minimumValue)
            {
                return true;
            }
            else
            {
                validationErrors.Add(this.GenerateValidationError(propertyName, string.Format("The value of {0} is less than the allowed value of {1}", propertyName, minimumValue)));
                return false;
            }
        }

        /// <summary>
        /// This method validates that the string is more than or equal to the minimum amount of characters
        /// </summary>
        /// <param name="propertyName">The name of the propety</param>
        /// <param name="propertyValue">The value of the property</param>
        /// <param name="minimumLength">The minimum length the string needs to be</param>
        /// <param name="validationErrors">The list of Validation Errors to be passed back</param>
        /// <returns>Returns a boolean based off of true or false</returns>
        public bool ValidateMinimumString(string propertyName, string propertyValue, int minimumLength, List<ValidationError> validationErrors)
        {
            if (propertyValue == null)
            {
                return true;
            }

            if (propertyName.Length >= minimumLength)
            {
                return true;
            }
            else
            {
                validationErrors.Add(this.GenerateValidationError(propertyName, string.Format("The value of {0} must have at least {1} characters.", propertyName, minimumLength)));
                return false;
            }
        }

        /// <summary>
        /// This method validates that the objects is not null
        /// </summary>
        /// <param name="propertyName">The name of the property</param>
        /// <param name="propertyValue">The value of the property</param>
        /// <param name="validationErrors">The List of type Validation Error that will be passed back.</param>
        /// <returns>Returns a sucess boolean</returns>
        public bool ValidateRequiredObject(string propertyName, object propertyValue, List<ValidationError> validationErrors)
        {
            if (propertyValue == null)
            {
                validationErrors.Add(this.GenerateValidationError(propertyName, string.Format("{0} is a required field.", propertyName)));
                return false;
            }
            else
            {
                return true;
            }
        }

        /// <summary>
        /// Validates a lookup off of a dictionary that is passed to it. 
        /// </summary>
        /// <param name="propertyName">The name of the property</param>
        /// <param name="propertyValue">The value of the property</param>
        /// <param name="listName">The name of the list that it must validate set by the attribute</param>
        /// <param name="validationErrors">A reference to a list of type ValidationError</param>
        /// <returns>Returns true or false depending if it properly validates</returns>
        public bool ValidateLookUp(string propertyName, object propertyValue, string listName, List<ValidationError> validationErrors)
        {
            if (propertyValue == null)
            {
                return true;
            }

            var validationList = new List<object>();

            try
            {
                validationList = this.Lists[listName];
            }
            catch
            {
                validationErrors.Add(this.GenerateValidationError(propertyName, string.Format("The LookUp List {0} does not exist in the current configuration.", listName)));
            }

            var lookups = validationList.Where(i => i.ToString() == propertyValue.ToString());

            if (!lookups.Any())
            {
                validationErrors.Add(this.GenerateValidationError(propertyName, string.Format("The value of {0} does not exist in the validation list {1}.", propertyValue, listName)));
                return false;
            }
            else if (lookups.Count() == 1)
            {
                return true;
            }
            else
            {
                validationErrors.Add(this.GenerateValidationError(propertyName, string.Format("The LookUp List {0} contains duplicate keys.", listName)));
                return false;
            }
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// A private method called Generate Validation Error which creates a new Validation Error 
        /// </summary>
        /// <param name="propertyName">The Name of the Property that errored</param>
        /// <param name="errorMessage">The message that will be displayed to the user.</param>
        /// <returns>Returns a ValidationError</returns>
        private ValidationError GenerateValidationError(string propertyName, string errorMessage)
        {
            var message = string.Format("[VALIDATION ERROR] Field: {0}, Message: {1}", propertyName, errorMessage);
            return new ValidationError() { Message = message, PropertyName = propertyName };
        }

        #endregion
    }
}